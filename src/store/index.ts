import Vue from 'vue'
import Vuex from 'vuex'
import Api from "@/utils/Api";

Vue.use(Vuex);

let store = new Vuex.Store({
  state: {
    accessToken: null,
    user: null
  },
  mutations: {
    initialiseStore(state) {
      // Check if the ID exists
      if (localStorage.getItem('store')) {
        // Replace the state object with the stored item
        this.replaceState(
            Object.assign(state, JSON.parse(<string>localStorage.getItem('store')))
        );
        loadUser();
      }
    },
    setToken(state, token)  {
      state.accessToken = token;
      loadUser();
    },

    setUser(state,user){
      state.user = user
    },

    emptyUser(state){
      state.user = null;
    },
    emptyToken(state) {
      state.accessToken = null;
      store.commit("emptyUser")
    }
  },
  actions: {
  },
  modules: {
  }

});

function loadUser(){
  Api.getInstance().getActualUser().then((user)=>{
    store.commit('setUser',user)
  });

}

store.subscribe((mutation, state) => {
  // Store the state object as a JSON string
  localStorage.setItem('store', JSON.stringify(state));
});

export default store;
