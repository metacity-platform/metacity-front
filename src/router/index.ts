import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from "@/store";

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/login',
    name: 'Connection',
    component: () => import('../views/Connection.vue')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import( '../views/About.vue')
  },
  {
    path: '/catalogue',
    name: 'catalogue',
    component: () => import( '../views/Catalogue.vue')
  },
  {
    path: '/creation',
    name: 'create',
    component: () => import( '../views/Create.vue')
  },
  {
    path: '/chart/:firstId',
    name: 'createChart',
    component: () => import('../components/createChart/CreateChart.vue'),
    props: true
  },
  {
    path: '/chart/:firstId/secondId',
    name: 'createCombinedChart',
    component: () => import('../components/createChart/CreateChart.vue'),
    props: true
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (!store.state.accessToken && to.path != '/login') next('/login');
  else next()
});

export default router
