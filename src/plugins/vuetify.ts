import Vue from 'vue';
import Vuetify, {VNavigationDrawer,VSnackbar} from 'vuetify/lib';
import fr from 'vuetify/src/locale/fr'

const vuetifyOptions = {
    components:{
      VNavigationDrawer,
      VSnackbar
    },
};

Vue.use(Vuetify,vuetifyOptions);

export default new Vuetify({
    lang: {
        locales: { fr },
        current: 'fr',
    },
    theme: {
        dark: false,
    },
});
