export class ColorTheme {
    name: string;
    colors: Array<string>;
    static readonly themes = [
        new ColorTheme('Metagray', ['#CCCCCC', '#AAAAAA', '#999999', '#666666', '#444444']),
        new ColorTheme('Metablues', ['#3F51B5', '#2196F3', '#00BCD4', '#009688', '#673AB7']),
        new ColorTheme('MetabluesInvert', ['#673AB7', '#009688', '#00BCD4', '#2196F3', '#3F51B5'])

    ];

    constructor(name: string, colors: Array<string> | []) {
        this.name = name;
        this.colors = colors
    }

    static getColorThemeFromName(name: string): ColorTheme {
        var theme = ColorTheme.themes.find(theme => theme.name === name);
        if (theme) {
            return theme;
        } else {
            throw new Error();
        }
    }

    static LightenDarkenColor(col: string,amt: number, alpha = 1) {
        if ( col[0] == "#" ) {
            col = col.slice(1);
        }

        let num = parseInt(col,16);

        let r = (num >> 16) + amt;

        if ( r > 255 ) r = 255;
        else if  (r < 0) r = 0;

        let b = ((num >> 8) & 0x00FF) + amt;

        if ( b > 255 ) b = 255;
        else if  (b < 0) b = 0;

        let g = (num & 0x0000FF) + amt;

        if ( g > 255 ) g = 255;
        else if  ( g < 0 ) g = 0;

        return 'rgba('+r+','+b+','+g+','+alpha+')';
    }

    generateGradientForXColors(numberOfColorToGenerate: number): string[] {
        let gradientColors: string[] = [];
        let colorPositions = [Math.round(numberOfColorToGenerate / 4), Math.round(numberOfColorToGenerate / 2), Math.round(3 * numberOfColorToGenerate / 4), numberOfColorToGenerate];
        for (let index = 0; index < colorPositions.length; index++) {
            let colorPosition = colorPositions[index];
            let newColors = this.generateColor(this.colors[index + 1], this.colors[index], colorPosition - (index > 0 ? colorPositions[index - 1] : 0));
            gradientColors = gradientColors.concat(newColors);
        }
        return gradientColors;
    }

    hex(c: number) {
        let s = "0123456789abcdef";
        if (c == 0 || isNaN(c))
            return "00";
        c = Math.round(Math.min(Math.max(0, c), 255));
        return s.charAt((c - c % 16) / 16) + s.charAt(c % 16);
    }

    /* Convert an RGB triplet to a hex string */
    convertToHex(rgb: Array<number>) {
        return '#' + this.hex(rgb[0]) + this.hex(rgb[1]) + this.hex(rgb[2]);
    }

    /* Remove '#' in color hex string */
    trim(s: string) {
        return (s.charAt(0) == '#') ? s.substring(1, 7) : s
    }

    /* Convert a hex string to an RGB triplet */
    convertToRGB(hex: string) {
        let color = [];
        color[0] = parseInt((this.trim(hex)).substring(0, 2), 16);
        color[1] = parseInt((this.trim(hex)).substring(2, 4), 16);
        color[2] = parseInt((this.trim(hex)).substring(4, 6), 16);
        return color;
    }

    generateColor(colorStart: string, colorEnd: string, colorCount: number) {

        // The beginning of your gradient
        let start = this.convertToRGB(colorStart);

        // The end of your gradient
        let end = this.convertToRGB(colorEnd);

        // The number of colors to compute
        let len = colorCount;

        //Alpha blending amount
        let alpha = 0.0;

        let saida = [];

        for (let i = 0; i < len; i++) {
            let c: number[] = [];
            alpha += (1.0 / len);

            c[0] = start[0] * alpha + (1 - alpha) * end[0];
            c[1] = start[1] * alpha + (1 - alpha) * end[1];
            c[2] = start[2] * alpha + (1 - alpha) * end[2];

            saida.push(this.convertToHex(c));

        }

        return saida;

    }
}
"#31579"
