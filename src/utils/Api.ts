import axios from "axios"
import Dataset from "@/utils/models/Dataset";
import Constants from "@/utils/Constants";
import {ChartType, ChartTypeUtil} from "./models/ChartType";
import {BodyDataset} from "./models/DataRequestBody";
import store from '@/store';
import router from "@/router";
import {User} from "./models/User";
import {Analyse} from "./models/Analyse";

export default class Api {
    private static instance: Api;

    private constructor() {
    }

    public static getInstance(): Api {
        if (!Api.instance) {
            Api.instance = new Api();
        }
        return Api.instance;
    }

    public static getAxiosConfig() {
        return {headers: {authorization: "Bearer " + store.state.accessToken}};
    }

    private static handleError(error: any) {
        if (error.response.status === 401) {
            router.push('/login');
        }
    }

    public async getAllDatasets(): Promise<Array<Dataset>> {
        let array = new Array<Dataset>();
        await axios.get(Constants.apiUrl + "datasets/all", Api.getAxiosConfig()).then((response) => {
            let data = response.data as Array<any>;
            data.forEach((element) => {
                array.push(Object.assign(new Dataset(), element))
            })
        });

        return array;
    }

    public async getDatasetsByFilters(Filters: Object): Promise<Array<Dataset>> {
        let array = new Array<Dataset>();
        let data;

        // @ts-ignore
        if (Filters.selectedGeojson == null) {
            // @ts-ignore
            data = {themeName: Filters.themesSelected}
        } else {
            // @ts-ignore
            data = {geojson: Filters.selectedGeojson, themeName: Filters.themesSelected}
        }
        await axios.post(Constants.apiUrl + "datasets/all", data, Api.getAxiosConfig()).then((response) => {
            let data = response.data as Array<any>;
            data.forEach((element) => {
                array.push(Object.assign(new Dataset(), element))
            })
        });

        return array;
    }

    public async getAnalysisByFilters(Filters: Object): Promise<Array<Analyse>> {
        let array = new Array<Analyse>();
        let data;

        // @ts-ignore
        if (Filters.selectedGeojson == null) {
            // @ts-ignore
            data = {themeName: Filters.themesSelected}
        } else {
            // @ts-ignore
            data = {geojson: Filters.selectedGeojson, themeName: Filters.themesSelected}
        }
        await axios.post(Constants.apiUrl + "analyse/all", data, Api.getAxiosConfig()).then((response) => {
            let data = response.data as Array<any>;
            data.forEach((element) => {
                array.push(Object.assign(new Analyse(), element))
            })
        });

        return array;

    }

    public async getAllRepresentations(): Promise<Array<ChartType>> {
        let array = new Array<ChartType>();
        await axios.get(Constants.apiUrl + "representationTypes", Api.getAxiosConfig()).then((response) => {
            let data = response.data as Array<any>;
            data.forEach((element) => {
                let chartType = ChartTypeUtil.getChartTypeFromName(element.name);
                if (chartType) {
                    array.push(chartType)
                }
            })
        }).catch(Api.handleError);
        return array;
    }

    public async getDatasetData(datasetName: string): Promise<Array<Object>> {
        let array = new Array<Object>();
        await axios.get(Constants.apiUrl + "index/get/" + datasetName + "/200000", Api.getAxiosConfig()).then((response) => {
            let data = response.data["hits"]["hits"];
            data.forEach((element: any) => {
                array.push(element["_source"])
            })
        }).catch(Api.handleError);
        return array;
    }



    public async getColumnsFromDataset(dataset: Dataset): Promise<Array<ChartType>> {
        let array = new Array<ChartType>();
        await axios.get(Constants.apiUrl + "index/accessiblefields/" + dataset.databaseName, Api.getAxiosConfig()).then((response) => {
            array = response.data as Array<any>;
        });
        return array;
    }

    public async getDataFromDatasets(body: BodyDataset): Promise<Object> {
        let data = {};
        await axios.post(Constants.apiUrl + "index/join", body, Api.getAxiosConfig()).then((response) => {
            data = response.data;
        });
        return data;
    }

    public async getDataset(datasetId: number): Promise<Dataset> {
        let dataset = new Dataset();
        await axios.get(Constants.apiUrl + "dataset/" + datasetId, Api.getAxiosConfig()).then(response => {
            dataset = Object.assign(new Dataset(), response.data[0])
        });
        return dataset;
    }


    public async getAllAnalysis():Promise<Array<Dataset>>{
            let array = new Array<Dataset>();
        await axios.get(Constants.apiUrl+"analyse/all", Api.getAxiosConfig()).then((response)=>{
            let data = response.data as Array<any>;
            data.forEach((element)=>{
                array.push(Object.assign(new Dataset(),element))
            })
        }).catch(Api.handleError);
        return array;
    }

    public async getThemes():Promise<Array<Object>>{
            let array = new Array<Object>();
        await axios.get(Constants.apiUrl+"theme/",Api.getAxiosConfig()).then((response)=>{
            array = response.data;
        }).catch(Api.handleError);
        return array;
    }

    public async login(mail:string, password:string): Promise<any> {
        let data: any;
        let body = {mail: mail, password: password};
        await axios.post(Constants.apiUrl + "login", {
            mail: mail,
            password: password
        }, Api.getAxiosConfig()).then((response) => {
            data = response.data["token"]["accessToken"];
        }).catch(Api.handleError);
        return data;
    }

    public async getActualUser(): Promise<User> {
        let user: any;
        await axios.get(Constants.apiUrl + "self", Api.getAxiosConfig()).then((response) => {
            user = Object.assign(new User(), response.data)
        }).catch(Api.handleError);
        return user;
    }
}
