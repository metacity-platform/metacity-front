import {StatType, StatTypeUtils} from "@/utils/models/StatType";
import {ChartType} from "@/utils/models/ChartType";

export class DataSorter {
    static simpleSort(data: {}, xColumn: string, columnsToGet: Array<String>, statsToGet: Array<[string, StatType]> | null) {
        // @ts-ignore
        let sortedData = new Object( {
            labels: [],
            datasets: []
        });
        if (statsToGet!= null && statsToGet.length > 0) {
            statsToGet.forEach((statsColumn, index) => {
                // @ts-ignore
                if (sortedData.datasets.length <= index) {
                    // @ts-ignore
                    let dataset = {
                        label: StatTypeUtils.getDisplayName(statsColumn[1].toString()) + ": " + DataSorter.trimLabel(statsColumn[0]),
                        data: []
                    };
                    // @ts-ignore
                    sortedData.datasets.push(dataset);
                    // @ts-ignore
                }
            });
            Object.keys(data).forEach(key => {
                if (key !== "###TOTAL###") {
                    // @ts-ignore
                    sortedData.labels.push(DataSorter.trimLabel(key));
                    // @ts-ignore
                    let line = data[key];
                    statsToGet.forEach((stats, index) => {
                        // @ts-ignore
                        sortedData.datasets[index].data.push(line.stats[stats[0]][stats[1]]);
                    })
                }
            })
        } else {
            // @ts-ignore
            data.forEach(line => {
                // @ts-ignore
                let key = "";
                // @ts-ignore
                if(xColumn.split(".")[0] == "properties") {
                    // @ts-ignore
                    key = line['properties'][xColumn.split(".")[1]];
                } else {
                    // @ts-ignore
                    key = line[xColumn];
                }
                // @ts-ignore
                sortedData.labels.push(key);
                columnsToGet.forEach((column, index) => {
                    // @ts-ignore
                    if (sortedData.datasets.length <= index) {
                        let dataset = {
                            label: DataSorter.trimLabel(column),
                            data: []
                        };
                        // @ts-ignore
                        sortedData.datasets.push(dataset);
                        // @ts-ignore
                    }
                    if(column.split(".")[0] == "properties") {
                        // @ts-ignore
                        sortedData.datasets[index].data.push(line["properties"][column.split(".")[1]]);
                    } else {
                        // @ts-ignore
                        sortedData.datasets[index].data.push(line[column])
                    }
                });
            });
        }
        return sortedData;
    }

    static columnSort(data: Object, statsToGet: Array<[string, StatType]>) {
        let sortedData = {
            labels: new Array<string>(),
            datasets: new Array<Object>()

        };
        let dataset = {
            label: new Array<string>(),
            data: new Array<any>()
        };
        dataset.label.push(StatTypeUtils.getDisplayName(statsToGet[0][1].toString()));
        statsToGet.forEach((statColumn) => {
            // @ts-ignore
            sortedData.labels.push(DataSorter.trimLabel(statColumn[0]));
            // @ts-ignore
            dataset.data.push(data["###TOTAL###"]["stats"][statColumn[0]][statColumn[1]]);
            // @ts-ignore
        });
        sortedData.datasets.push(dataset);

        return sortedData;
    }

    static linearSort(data : Array<any>, xColumn: string, columnsToGet : Array<String>, statsToGet: Array<[string, StatType]>|null): Object|Array<Object> {
        let presortedData = {};
        if(statsToGet != null && statsToGet.length > 0) {
            let sortedKeys = Object.keys(data).sort();
            sortedKeys.forEach(key => {
                // @ts-ignore
                presortedData[key] = data[key];
            })
        } else {
            presortedData = data.sort((a, b) => {
                if (a[xColumn] < b[xColumn])
                    return -1;
                else if (a[xColumn] > b[xColumn])
                    return 1;
                else
                    return 0;
            });
        }
        return this.simpleSort(presortedData, xColumn, columnsToGet, statsToGet);
    }

    private static trimLabel(label: String|string): String|string {
        return label.indexOf('.') === -1 ? label : label.split('.')[1]
    }
}
