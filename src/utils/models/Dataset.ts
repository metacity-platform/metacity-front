import axios from "axios";
import Constants from "@/utils/Constants";
import Api from "@/utils/Api";
export default class Dataset{
    id! : number;
    name! : string;
    databaseName! : string;
    validated! : boolean;
    description! : string;
    creator!: string;
    contributor!: string;
    license! : string;
    created_date! : string;
    updated_date!: string;
    realtime!: boolean;
    conf_ready!:boolean;
    upload_ready!:boolean;
    open_data!:boolean;
    visibility!:string;
    user!:string;
    JSON!:boolean;
    GEOJSON!:boolean;
    producer!:string;
    themeName!:string;
    update_frequency!:string;
    created_at!:string;
    updated_at!:string;
    favorite!:boolean;
    numberOfLines = 0;

    constructor() {

    }

    getNumberOfLineFromApi(){
        if(this.numberOfLines != 0){
            return
        }
        return axios.get(Constants.apiUrl+"index/get/"+this.databaseName+"/1",Api.getAxiosConfig()).then((response)=>{
            this.numberOfLines = response.data.hits.total.value
        })
    }

    toggleFavorited(){
        if(this.favorite){
            return axios.get(Constants.apiUrl+"dataset/"+this.id+"/unfavorite",Api.getAxiosConfig()).then(()=>{
                this.favorite = false;
            });
        }
        return axios.get(Constants.apiUrl+"dataset/"+this.id+"/favorite",Api.getAxiosConfig()).then(()=>{
            this.favorite = true;
        });
}
}
