export enum StatType {
    min,
    max,
    avg,
    sum,
    count,
    DiffOcc,
    DiffSum,
    DiffAvg,
    FilCount,
    FilSum,
    FilAvg
}

export class StatTypeUtils {
    static getDisplayName(statType: string) : string {
        switch (statType) {
            case "min":
                return "min";
            case "max":
                return "max";
            case "avg":
                return "moyenne";
            case "sum":
                return "somme";
            case "count":
                return "compte";
            case "DiffOcc":
                return "compte diff.";
            case "DiffSum":
                return "somme diff.";
            case "DiffAvg":
                return "moyenne diff.";
            case "FilCount":
                return "ERROR";
            case "FilSum":
                return "ERROR";
            case "FilAvg":
                return "ERROR";
            default:
                return "ERROR DEF"

        }
    }

}
