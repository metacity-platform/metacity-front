export class User {

    user_id?:Number;
    firstname?:string;
    lastname?:string;
    role?:string;
    service?:string;
    direction?:string;
    mail?:string;
    phone?:string;
    token?:string;
    token_expiration?:string;
    created_at?:string;
    updated_at?:string;

    constructor() {
    }
}