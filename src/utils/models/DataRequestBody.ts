export class DataRequestBody {
    datasets: Array<BodyDataset>;
    joining: Array<Array<string>>;
    stats: Stats;

    constructor() {
        this.stats = new Stats();
        this.datasets = [];
        this.joining = [];
    }
}

export class Stats {
    // tslint:disable-next-line:variable-name
    do_stats: boolean;
    columns: StatsColumn|undefined;

    constructor() {
        this.do_stats = false;
        this.columns = undefined;
    }
}

export class StatsColumn {
    isDate: boolean;
    pivot: string|undefined;
    step: number|undefined;
    data: Array<string>|undefined;

    constructor() {
        this.isDate = false;
    }
}

export class BodyDataset {
    name: string | undefined;
    columns: Array<string> | undefined;
    size: number;
    offset: number;
    // tslint:disable-next-line:variable-name
    date_col: string|null;
    // tslint:disable-next-line:variable-name
    start_date: string|null;
    // tslint:disable-next-line:variable-name
    end_date: string|null;
    weekdays: Array<string>|null;
    // tslint:disable-next-line:variable-name
    start_minute: string|null;
    // tslint:disable-next-line:variable-name
    end_minute: string|null;

    constructor() {
        this.size = 300000;
        this.offset = 0;
        this.date_col = null;
        this.start_date = null;
        this.end_date = null;
        this.weekdays = null;
        this.start_minute = null;
        this.end_minute = null;
    }
}
