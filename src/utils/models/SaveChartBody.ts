import {DataRequestBody} from "@/utils/models/DataRequestBody";
import {StatType} from "@/utils/models/StatType";
import {ChartType} from "@/utils/models/ChartType";

export class SaveChartBody {
    chartType: ChartType | undefined;
    sortType: SortType | undefined;
    columnsToGet: Array<string>| undefined;
    statsToGet: Array<[string, StatType]> | null | undefined;
    dataRequestBody: DataRequestBody | null;
    chartOptions: Object|null;

    constructor() {
        this.dataRequestBody = null;
        this.chartOptions = null;
    }
}

export enum SortType {
    simple,
    column,
    linear
}
