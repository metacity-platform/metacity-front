export enum ChartType {
    line = "Graphique en courbes",
    bar = "Graphique en barres",
    radar = "Graphique en radar",
    pie = "Graphique en secteur",
    donut = "Graphique en anneau",
    polar = "Graphique polaire",
    column = "Graphique en colonnes",
}

export class ChartTypeUtil {
    static getChartTypeFromName(name: string) : ChartType|undefined {
        switch (name) {
            case "Graphique en courbes":
                return ChartType.line;
            case "Graphique en barres":
                return ChartType.bar;
            case "Graphique en radar":
                return ChartType.radar;
            case "Graphique en secteur":
                return ChartType.pie;
            case "Graphique en anneau":
                return ChartType.donut;
            case "Graphique polaire":
                return ChartType.polar;
            case "Graphique en colonnes":
                return ChartType.column;
            default:
                return undefined;
        }
    }

    static getUnselectedIcon(chartType : ChartType) : string {
        switch (chartType) {
            case ChartType.line:
                return require("@/assets/images/upload/statistics-line-unselect.svg");
            case ChartType.bar:
                return require("@/assets/images/upload/statistics-bar-chart-unselect.svg");
            case ChartType.radar:
                return require("@/assets/images/upload/statistics-radar-unselect.svg");
            case ChartType.pie:
                return require("@/assets/images/upload/statistics-camembert-unselect.svg");
            case ChartType.donut:
                return require("@/assets/images/upload/statistic-donut-unselect.svg");
            case ChartType.polar:
                return require("@/assets/images/upload/statistics-radar-polaire-unselect.svg");
            case ChartType.column:
                return require("@/assets/images/upload/statistics-column-unselect.svg");
        }
    }

    static getSelectedIcon(chartType : ChartType) : string {
        switch (chartType) {
            case ChartType.line:
                return require("@/assets/images/upload/statistics-line-selected.svg");
            case ChartType.bar:
                return require("@/assets/images/upload/statistics-bar-chart-selected.svg");
            case ChartType.radar:
                return require("@/assets/images/upload/statistics-radar-selected.svg");
            case ChartType.pie:
                return require("@/assets/images/upload/statistics-camembert-selected.svg");
            case ChartType.donut:
                return require("@/assets/images/upload/statistic-donut-selected.svg");
            case ChartType.polar:
                return require("@/assets/images/upload/statistics-radar-polaire-selected.svg");
            case ChartType.column:
                return require("@/assets/images/upload/statistics-column-selected.svg");
        }
    }
}
