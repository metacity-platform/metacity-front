export class AnalyseColumn {
    field?: string;
    databaseName?: string;
    // tslint:disable-next-line:variable-name
    color_code?: string;
    usage?: string;

    constructor(field: string, databaseName: string, color_code: string, usage: string) {
        this.field = field;
        this.databaseName = databaseName;
        this.color_code = color_code;
        this.usage = usage;
    }
}
