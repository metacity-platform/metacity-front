import {AnalyseColumn} from './AnalyseColumn';
import {DataRequestBody} from './DataRequestBody';

export class Analyse {
    id?: number | null;
    name?: string;
    description?: string;
    // tslint:disable-next-line:variable-name
    representation_type?: string;
    shared?: boolean;
    visibility?: string;
    body?: DataRequestBody;
    isStats?: boolean;
    isOwner?: boolean|undefined;
    // tslint:disable-next-line:variable-name
    theme_name?: string;
    isMap?: boolean;
    usage?: string;
    // tslint:disable-next-line:variable-name
    analysis_column?: Array<AnalyseColumn>;

    constructor(name? : string, description? : string, representation_type? : string, shared? : boolean,
                visibility? : string, body? : DataRequestBody, isStats? : boolean, theme_name? : string, isMap? : boolean, usage? : string, analysis_column? : Array<AnalyseColumn>) {
        this.id = null;
        this.name = name;
        this.description = description;
        this.representation_type = representation_type;
        this.shared = shared;
        this.visibility = visibility;
        this.body = new DataRequestBody();
        if (body) {
            this.body.datasets = body.datasets;
            this.body.stats = body.stats;
            this.body.joining = body.joining;
        }
        this.isStats = isStats;
        this.theme_name = theme_name;
        this.isMap = isMap;
        this.usage = usage;
        this.analysis_column = analysis_column;
    }

}
